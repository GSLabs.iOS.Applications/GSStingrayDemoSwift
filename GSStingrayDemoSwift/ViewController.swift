//
//  ViewController.swift
//  GSStingrayDemoSwift
//
//  Created by Dan on 23.11.2021.
//

import UIKit

class ViewController: UIViewController, StgBrowserDelegate {
    var browser: StgBrowser!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        browser = StgBrowser(type: "_stingray-remote._tcp")
        browser.delegates.add(self)
        browser.start()
    }
    
    func stgBrowser(_ sender: StgBrowser!, endpointFound endpoint: StgEndpoint!) {
        sender.stop()
        
        let client = StgClient(endpoint: endpoint, userAgent: "stingray")!
        powerSet(client: client)
//        channelsGet(client: client)
//        channelSet(client: client)
    }
    
    func powerSet(client: StgClient) {
        client.powerSetAsync(true) { error in
            if error == nil {
                print("Powered on")
            } else {
                print("Error - \(error!.localizedDescription)")
            }
        }
    }
    
    func channelsGet(client: StgClient) {
        client.channelsGetAsync { channels, error in
            if channels == nil {
                print("Error - \(error!.localizedDescription)")
            } else {
                print("\(channels!.count) channels available")
            }
        }
    }
    
    func channelSet(client: StgClient) {
        let channel = StgChannel()
        channel.channelListId = "TV"
        channel.channelNumber = 2
        
        client.channelSetAsync(channel) { error in
            if error == nil {
                print("Channel changed to - \(channel.channelNumber)")
            } else {
                print("Error - \(error!.localizedDescription)")
            }
        }
    }
}
